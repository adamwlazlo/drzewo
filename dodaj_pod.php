<?php

require_once 'database.php';


if(isset($_POST['nazwa']))
{

    $id_rodzica = isset($_GET['id_rodzica']) ? intval($_GET['id_rodzica']) : 0;
    $poziom = isset($_GET['poziom']) ? intval($_GET['poziom']) : 0;

    if (($id_rodzica > 0)||($poziom > 0))
    {
        $nazwa=$_POST['nazwa'];
        $katalog = $db->prepare("SELECT nazwa FROM kategorie WHERE nazwa=:nazwa AND poziom=:poziom AND id_rodzica=:id_rodzica"); //zapytanie
        $katalog->bindValue(':nazwa', htmlentities($nazwa, ENT_QUOTES, "UTF-8"), PDO::PARAM_STR);
        $katalog->bindValue(':poziom', htmlentities($poziom, ENT_QUOTES, "UTF-8"), PDO::PARAM_STR);
        $katalog->bindValue(':id_rodzica', htmlentities($id_rodzica, ENT_QUOTES, "UTF-8"), PDO::PARAM_STR);
        $katalog->execute();
        $katalog_ist = $katalog->fetch();

        if($katalog_ist) // jesli podany katalog istnieje w tym miejsu to wracamy do index.html i wyswietlamy komunikat
        {
            $_SESSION['kat_istnieje']="Istnieje już taki katalog w tym folderze";
            header('Location: index.php');
            exit();
        }
        else
        {
            $dodaj = $db->prepare('INSERT INTO `kategorie` (`id`,`nazwa`,`poziom`,`id_rodzica`) VALUES (NULL,:nazwa,:poziom,:id_rodzica)');
            $dodaj->bindParam( ':nazwa', htmlentities($_POST['nazwa'], ENT_QUOTES, "UTF-8"));
            $dodaj->bindParam( ':poziom', htmlentities($_GET['poziom'], ENT_QUOTES, "UTF-8"));
            $dodaj->bindParam( ':id_rodzica', htmlentities($_GET['id_rodzica'], ENT_QUOTES, "UTF-8"));
            $dodaj->execute();

            header('Location: index.php');
        }
    }
}
echo "rodzic ".$_GET['id_rodzica'].'<br />';
echo "poziom ".$_GET['poziom'];
echo '
<form method="POST" action="dodaj_pod.php?id_rodzica='.$_GET['id_rodzica'].'&poziom='.$_GET['poziom'].'">
Podaj nazwe katalogu:
<input type="text" name="nazwa" />
<input type="submit" value="Dodaj podkatalog" />
';