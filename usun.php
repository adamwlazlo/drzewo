<?php

require_once 'database.php';

$id = isset($_GET['id']) ? intval($_GET['id']) : 0; //intval parsuje zmienna id z geta na inta sby nie było możliwosci dodawania podejzanych znakow

if ($id > 0)
{
    $do_usun = $db->prepare('DELETE FROM kategorie WHERE id = :id OR id_rodzica = :idr');
    $do_usun->bindParam(':id', $id);
    $do_usun->bindParam(':idr', $id);
    $do_usun->execute();

    header('Location: index.php');
}
else
{
    header('Location: index.php');
}