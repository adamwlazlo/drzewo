<?php

session_start();

$config = require_once 'config.php';

try
{
    $db = new PDO("mysql:host={$config['host']};dbname={$config['database']};charset=utf8", $config['user'], $config['password'], [
        PDO::ATTR_EMULATE_PREPARES => false,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC //wyświetlanie tylko jako tablica asocjacyjna 
    ]);


    $_SESSION['info'] = 'połączono z bazą '.$config['database'];

}
catch (PDOException $error)
{
    echo $error->getMessage();
    exit('connection error');
}
