<?php

require_once 'database.php';

if(isset($_POST['nazwa']))
{
    $nazwa=$_POST['nazwa'];
    $katalog = $db->prepare("SELECT nazwa FROM kategorie WHERE nazwa=:nazwa AND poziom=0"); //zapytanie
    $katalog->bindValue(':nazwa', htmlentities($nazwa, ENT_QUOTES, "UTF-8"), PDO::PARAM_STR);//dane do zapytania
    $katalog->execute();//wykonanie zapytania
    //echo $userQuery->rowCount();
    $katalog_ist = $katalog->fetch();

    if($katalog_ist) // jesli podany katalog istnieje na tym poziomie to wracamy do index.html i wyswietlamy komunikat
    {
        $_SESSION['kat_istnieje']="Istnieje już taki katalog na tym poziomie";
        header('Location: index.php');
        exit();
    }
    else
    {
        $dodaj = $db->prepare('INSERT INTO `kategorie` (`id`,`nazwa`,`poziom`,`id_rodzica`) VALUES (NULL,:nazwa,0,0)');
        $dodaj->bindParam( ':nazwa', htmlentities($_POST['nazwa'], ENT_QUOTES, "UTF-8"));
        $dodaj->execute();

        header('Location: index.php');
    }
}

echo '
<form method="POST" action="dodaj.php">
Podaj nazwe katalogu:
<input type="text" name="nazwa" />
<input type="submit" value="Dodaj" />

';
